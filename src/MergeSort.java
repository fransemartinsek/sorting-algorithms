public class MergeSort {
	
	/**Merge sort an int array
	 * @param arr initial array
	 * @param l left border
	 * @param r right border
	 * @param order flag for sorting order(true=ascending)	
	 */
	void sort(int[] arr, int l, int r, boolean order) {
		if(r <= l) return;
		
		int m = (l+r)/2;
		sort(arr, l, m, order);
		sort(arr, m+1, r, order);
		
		merge(arr, l, m, r, order);
	}
	
	/**Merge sort a String array by length
	 * @param arr initial array
	 * @param l left border
	 * @param r right border
	 * @param order flag for sorting order(true=ascending)
	 */
	void sort(String[] arr, int l, int r, boolean order) {
		if(r <= l) return;
		
		int m = (l+r)/2;
		sort(arr, l, m, order);
		sort(arr, m+1, r, order);
		
		merge(arr, l, m, r, order);
	}
	
	
	/**Merges two subarrays of an int array
	 * @param arr initial array
	 * @param l starting index of left subarray
	 * @param m border between the subarrays
	 * @param r ending index of right subarray
	 * @param order flag for sorting order(true=ascending)
	 */
	void merge(int[] arr, int l, int m, int r, boolean order) {
		int size1 = m - l + 1;
		int size2 = r - m;

		int[] left = new int[size1];
		int[] right = new int[size1];
		
		for(int i = 0; i < size1; i++) {
			left[i] = arr[l+i];
		}
		for(int i = 0; i < size2; i++) {
			right[i] = arr[m+i+1];
		}
		
		int i = 0, j = 0, k = l;
		while(i < size1 && j < size2) {
			boolean compare = order ? left[i] < right[j] : left[i] >= right[j];
			if(compare) {
				arr[k] = left[i];
				i++;
			}else {
				arr[k] = right[j];
				j++;
			}
			k++;
		}
		
		while(i < size1) {
			arr[k] = left[i];
			k++;
			i++;
		}
		
		while(j < size2) {
			arr[k] = right[j];
			k++;
			j++;
		}
	}
	/**Merges two subarrays of a String array while using the string length to compare
	 * @param arr initial array
	 * @param l starting index of left subarray
	 * @param m border between the subarrays
	 * @param r ending index of right subarray
	 * @param order flag for sorting order(true=ascending)
	 */
	void merge(String[] arr, int l, int m, int r, boolean order) {
		int size1 = m - l + 1;
		int size2 = r - m;

		String[] left = new String[size1];
		String[] right = new String[size1];
		
		for(int i = 0; i < size1; i++) {
			left[i] = arr[l+i];
		}
		for(int i = 0; i < size2; i++) {
			right[i] = arr[m+i+1];
		}
		
		int i = 0, j = 0, k = l;
		while(i < size1 && j < size2) {
			boolean compare = order ? left[i].length() < right[j].length() : left[i].length() >= right[j].length();
			if(compare) {
				arr[k] = left[i];
				i++;
			}else {
				arr[k] = right[j];
				j++;
			}
			k++;
		}
		
		while(i < size1) {
			arr[k] = left[i];
			k++;
			i++;
		}
		
		while(j < size2) {
			arr[k] = right[j];
			k++;
			j++;
		}
	}
	
}
