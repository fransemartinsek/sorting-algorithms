import java.util.Random;

public class QuickSort {
	
	/**Method that sorts an array of type int using randomised quicksort
	 * @param arr initial array
	 * @param low lower boundary
	 * @param high upper boundary
	 * @param order sorting order(true=ascending)
	 */
	void sort(int arr[], int low, int high, boolean order) {
		if(low>=high)
			return;
		int l = low;
		int r = high;
		
		//choose a random pivot
		Random rand = new Random();
		int pivot = arr[rand.nextInt(high-low) + low];
		
		//increment left and right indices untill they meet
		while(l <= r) {
			//skip all correctly positioned elements and stop at first wrongly positioned element on both sides of pivot
			while(( order ? ( arr[l] < pivot ):( arr[l]>pivot )) ) {
				l++;
			}
			while(( order ? ( arr[r] > pivot ):( arr[r]<pivot )) ) {
				r--;
			}
			
			//if indices have not crossed swap the two elements and step forward once
			if(l<=r) {
				int temp = arr[l];
				arr[l] = arr[r];
				arr[r] = temp;
				l++;
				r--;
			}
		}
				
		//recursive calls to sort left and right subarrays
		sort(arr,low,r,order);
		sort(arr,l,high,order);
	} 
}
